﻿using MassTransit;
using RabbitMQ.Client;
using System;
using System.Threading.Tasks;

namespace Example1
{
    // Message Definition
    public class YourMessage
    {
        public string Text { get; set; }
    }

    class Program
    {
        static async Task Main(string[] args)
        {
            var busControl = Bus.Factory.CreateUsingRabbitMq(cfg =>
            {
                cfg.Host("localhost", h =>
                {
                    h.Username("guest");
                    h.Password("guest");
                });

                //cfg.Publish<YourMessage>(s => s.ExchangeType = ExchangeType.Direct);

                cfg.ReceiveEndpoint("input-queue", ep =>
                {
                    //ep.ConfigureConsumeTopology = false;
                    //ep.ExchangeType = ExchangeType.Direct;
                  
                    //ep.Bind("AddUser1", e =>
                    //{
                    //    e.ExchangeType = "direct";
                    //    e.RoutingKey = "test";
                    //});
                    ep.Handler<YourMessage>(context =>
                    {
                        return Console.Out.WriteLineAsync($"Received: {context.Message.Text}");
                    });
                });

            }
            );

            // Important! The bus must be started before using it!
            await busControl.StartAsync();
            try
            {
                do
                {
                    string value = await Task.Run(() =>
                    {
                        Console.WriteLine("Enter message (or quit to exit)");
                        Console.Write("> ");
                        return Console.ReadLine();
                    });

                    if ("quit".Equals(value, StringComparison.OrdinalIgnoreCase))
                        break;
                    var sendToUri = new Uri($"queue:input-queue");
                    var endPoint = await busControl.GetSendEndpoint(sendToUri);
                    await endPoint.Send(new YourMessage
                    {
                        Text = value
                    });
                    //await busControl.Publish<YourMessage>(new
                    //{
                    //    Text = value
                    //});
                }
                while (true);
            }
            finally
            {
                await busControl.StopAsync();
            }
        }
    }
}
